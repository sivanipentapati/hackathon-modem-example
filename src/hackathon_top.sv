`default_nettype none

module hackathon_top (
  input  wire       CLOCK_50_B5B,
  input  wire       UART_RX,
  output wire       UART_TX,
  output wire       I2C_SCL,
  inout  wire       I2C_SDA,
  input  wire       AUD_ADCDAT,
  output wire       AUD_ADCLRCK,
  output wire       AUD_BCLK,
  output wire       AUD_DACDAT,
  output wire       AUD_DACLRCK,
  output wire       AUD_XCK
);

  wire        reset;
  wire [15:0] analog_data_i;
  wire        analog_ready_i;
  wire [15:0] analog_data_o;
  wire        analog_valid_o;
  wire  [7:0] digital_data_i;
  wire        digital_valid_i;
  wire        digital_ready_i;
  wire  [7:0] digital_data_o;
  wire        digital_valid_o;
  wire        digital_ready_o;
  
  hackathon_base hackathon_base_inst(
    .clk             (CLOCK_50_B5B),
    .reset_o         (reset),
    .analog_data_i   (analog_data_i),
    .analog_ready_i  (analog_ready_i),
    .analog_data_o   (analog_data_o),
    .analog_valid_o  (analog_valid_o),
    .digital_data_i  (digital_data_i),
    .digital_valid_i (digital_valid_i),
    .digital_ready_i (digital_ready_i),
    .digital_data_o  (digital_data_o),
    .digital_valid_o (digital_valid_o),
    .digital_ready_o (digital_ready_o),
    .uart_rx         (UART_RX),
    .uart_tx         (UART_TX),
    .i2c_scl         (I2C_SCL),
    .i2c_sda         (I2C_SDA),
    .aud_adcdat      (AUD_ADCDAT),
    .aud_adclrck     (AUD_ADCLRCK),
    .aud_bclk        (AUD_BCLK),
    .aud_dacdat      (AUD_DACDAT),
    .aud_daclrck     (AUD_DACLRCK),
    .aud_xck         (AUD_XCK)
  );

modem_wrapper modem(
  .clk              (CLOCK_50_B5B),
  .reset            (reset),
  .mod_symbol_data  (digital_data_o),
  .mod_symbol_valid (digital_valid_o),
  .mod_symbol_ready (digital_ready_o),
  .mod_iq_data      (analog_data_i),
  .mod_iq_ready     (analog_ready_i),
  .dem_iq_data      (analog_data_o),
  .dem_iq_valid     (analog_valid_o),
  .dem_symbol_data  (digital_data_i),
  .dem_symbol_valid (digital_valid_i),
  .dem_symbol_ready (digital_ready_i)
);

endmodule

`default_nettype wire
