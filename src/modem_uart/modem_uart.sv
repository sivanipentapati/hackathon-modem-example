
module modem_uart#(
  parameter DIV = 4,
  parameter FREQ = 16,
  parameter MOD_GAIN = 16'h4000,
  parameter DEM_GAIN = 16'h4000
) (
input wire clk,
input wire reset,

output reg  [15:0] filter_raw,

input wire  [ 7:0] mod_symbol_data,
input wire         mod_symbol_valid,
output reg         mod_symbol_ready,

output reg  [15:0] mod_iq_data,
output wire        mod_iq_valid,
input  wire        mod_iq_ready,

input  wire [15:0] dem_iq_data,
input  wire        dem_iq_valid,
output wire        dem_iq_ready,

output reg  [ 7:0] dem_symbol_data,
output reg         dem_symbol_valid,
input  wire        dem_symbol_ready
);

assign dem_iq_ready = 1'b1;
assign mod_iq_valid = 1'b1;

logic [DIV-1:0] div_cnt;
always_ff@(posedge clk) begin
  if(mod_iq_ready)
    div_cnt <= div_cnt + 1;
  if(reset)
    div_cnt <= '0;
end

logic [3:0] bit_cnt;
always_ff@(posedge clk) begin
  mod_symbol_ready <= 1'b0;
  if(&div_cnt && mod_iq_ready) begin
    bit_cnt <= bit_cnt + 1;
    if(bit_cnt==9) begin
      mod_symbol_ready <= 1'b1;
      bit_cnt <= '0;
    end
  end
  if(reset)
    bit_cnt <= '0;
end

logic [9:0] mod_data_buffer;
always_ff@(posedge clk) begin
  if(mod_symbol_ready)
    mod_data_buffer <= mod_symbol_valid ? {1'b0,mod_symbol_data,1'b1} : '1;
  if(reset)
    mod_data_buffer <= '1;
end

logic mod_symbol_bit;
always_ff@(posedge clk)
  mod_symbol_bit <= mod_data_buffer[9-bit_cnt];

logic [5:0] t = 0;
always_ff@(posedge clk)
  if(mod_iq_ready)
    t <= t + FREQ;

logic signed [15:0] carrier_tx;
sine#(.N(16)) sine_tx (
  .clk   (clk),
  .angle (t),
  .out   (carrier_tx)
);

logic [15:0] mod_iq_bb;
logic [15:0] mod_iq_bb_scaled;
assign mod_iq_bb = {mod_symbol_bit,{15{~mod_symbol_bit}}};
always_ff@(posedge clk) begin
  mod_iq_bb_scaled <= data_pkg::multiply_Q(MOD_GAIN,mod_iq_bb);
  mod_iq_data <= data_pkg::multiply_Q(carrier_tx,mod_iq_bb_scaled);
end

//----------DEMODULATOR
logic signed [15:0] carrier_rx;
sine#(.N(16)) sine_rx (
  .clk   (clk),
  .angle (t+2),
  .out   (carrier_rx)
);

logic signed [15:0] dem_pre_filter_data;
logic dem_pre_filter_valid;
always_ff@(posedge clk) begin
  dem_pre_filter_valid <= dem_iq_valid;
  dem_pre_filter_data <= data_pkg::multiply_Q(carrier_rx,dem_iq_data);
end

logic signed [15:0] dem_filter_data;
logic dem_filter_valid;

fir filter(
  .clk    (clk),
  .reset  (reset),
  .data_i (dem_pre_filter_data),
  .valid_i(dem_pre_filter_valid),
  .ready_i(),
  .data_o (dem_filter_data),
  .valid_o(dem_filter_valid),
  .ready_o(1'b1)
);

logic [15:0] dem_dec_data;
logic dem_dec_valid;

decimate#(
  .DIV_LOG2(DIV)
) decimate (
  .clk       (clk),
  .reset     (reset),
  .in_data   (dem_filter_data),
  .in_valid  (dem_filter_valid),
  .in_ready  (),
  .out_data  (dem_dec_data),
  .out_valid (dem_dec_valid),
  .out_ready (1'b1)
);

always_ff@(posedge clk)
  if(dem_dec_valid)
    filter_raw <= dem_dec_data;

logic rx_bit;
assign rx_bit = !dem_dec_data[15];

logic [7:0] rx_buf;
always_ff@(posedge clk)
if(dem_dec_valid) 
  rx_buf <= {rx_buf[6:0],rx_bit};

logic rx_bit_d;
logic rx_bit_valid;
logic rx_buf_valid;
logic [2:0] rx_cnt;
always_ff@(posedge clk) begin
  rx_buf_valid <= 1'b0;
  rx_bit_d <= rx_bit;
  if(dem_dec_valid) begin
  rx_cnt <= rx_bit_valid ? rx_cnt + 1 : '0;
    if(!rx_bit && rx_bit_d)
      rx_bit_valid <= 1'b1;
  if(rx_cnt==7) begin
    rx_bit_valid <= 1'b0;
    rx_buf_valid <= 1'b1;
  end
  end  
  if(reset)
    rx_bit_valid <= 1'b0;
end

assign dem_symbol_data = rx_buf;
assign dem_symbol_valid = rx_buf_valid;

endmodule
